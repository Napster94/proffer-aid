import RemoteConfigContext from '../context/remote-config-context'
import { useState, useEffect } from 'react'
import Loader from 'react-loader'

import firebase from 'firebase/app';
import 'firebase/remote-config';


export default function RemoteConfigsProvider(props) {

    // let provided_configs = 'null';

    const [ready, setReady] = useState(false);
    const [providedConfigs, setProvidedConfigs] = useState({});

    async function _init_() {

        if (process.browser) {
            try {
                const remoteConfig = new firebase.remoteConfig();

                console.log('running in dev');
                remoteConfig.settings = {
                    fetchTimeoutMillis: 60000,
                    minimumFetchIntervalMillis: 10// 3600000
                };

                //activate previously fetched configs
                remoteConfig.activate().then(() => {
                    setProvidedConfigs(remoteConfig.getAll());
                }).catch(error => {
                    console.log(error);
                })

                //get new remote configs
                remoteConfig.fetch().then(() => {
                    setProvidedConfigs(remoteConfig.getAll());
                }).catch((err) => {
                    console.error(err);
                });
                console.log(remoteConfig.getAll());

            } catch (error) {
                console.log(error);
            }
            setReady(true);
        }
    }

    useEffect(() => {
        _init_()
    }, [])

    return (
        <Loader loaded={ready}>
            <RemoteConfigContext.Provider value={{ REMOTE_CONFIGS: providedConfigs }}>
                {props.children}
            </RemoteConfigContext.Provider>
        </Loader>
    )
}
