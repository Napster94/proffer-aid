import { useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';

export default function TopNav(props) {

    const isDesktopOrLaptop = useMediaQuery({ minWidth: 992 });
    const isTabletOrMobile = useMediaQuery({ maxWidth: 991 });

    const [offCanvasOpen, setOffCanvasOpen] = useState(false);
    const [pageScrolled, setPageScrolled] = useState(false);

    useEffect(() => {
        console.log('render carousel');
        window.jQuery = require('../public/vendor/jquery/jquery.min.js');

        window.jQuery(window).scroll(function () {
            console.log('scrolling page...');
            window.jQuery(".navbar-fixed-top").toggleClass('scrolled', window.jQuery(this).scrollTop() > window.jQuery(".navbar-fixed-top").height());
        })

    }, [props.children]);

    const _toggleOffCanvas = (_state) => {
        if (process.browser) {
            console.log('in browser');
            document.getElementById('navBarMobile').classList.toggle("open");
        }
    }

    return (
        <nav className={`navbar navbar-expand-lg navbar-fixed-top fixed-top navbar-${props.theme} ${props.shadow ? 'shadow-sm' : ''}bg-${props.bgColor || 'dark'} ${props.className}`}>
            <style global jsx>
                {`
                    .navbar-fixed-top.scrolled {
                        background-color: ${props.scrollColor || 'inherit'} !important;
                        transition: background-color 200ms linear;
                    }
                `}
            </style>
            <a className="navbar-brand text-decoration-none mx-0" href="#">
                {props.logo &&
                    <img src={props.logo} width="50" height="50" className="mx-2 d-inline-block align-middle" alt="Agency Logo" />
                }
                {isTabletOrMobile ?
                    <>{`${props.name.length >= 17 ? `${props.name.substring(0, Math.min(17, props.name.length))}...` : props.name} `}</> :
                    props.name
                }
            </a>
            <button className="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas" onClick={() => { _toggleOffCanvas(!offCanvasOpen) }}>
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className={`${isTabletOrMobile ? `bg-${props.theme ? props.theme : 'dark'}` : ''} navbar-collapse offcanvas-collapse px-0`} id="navBarMobile">
                {isDesktopOrLaptop &&
                    <>
                        {props.left}
                        <div className="ml-auto">
                            {props.right}
                        </div>
                    </>
                }
                {isTabletOrMobile &&
                    props.mobileNav
                }
            </div>
        </nav>
    )
}