import Headfile from '../components/head-file';
import dynamic from 'next/dynamic';
import Image from 'next/image';
import Loader from 'react-loader';
import { useState } from 'react';
import Card from 'react-bootstrap/Card'
import TopNav from '../components/top-nav';
import { useMediaQuery } from 'react-responsive';

const OwlCarousel = dynamic(
  () => import('../components/owl-carousel'),
  { loading: () => <Loader loaded={false}></Loader>, ssr: false }
);

export default function Home() {
  const isDesktopOrLaptop = useMediaQuery({ minWidth: 992 });
  const isTabletOrMobile = useMediaQuery({ maxWidth: 991 });

  const [isReady, setReady] = useState(true);
  const [activeLink, setActiveLink] = useState('link-1');

  return (
    <Loader loaded={isReady}>
      <Headfile
        pageTitle={'Proffer Aid International Foundation | Racing to save lives'}
      />
      <TopNav
        bgColor={'transparent'}
        theme={'light'}
        shadow={false}
        logo={'/img/logo.png'}
        scrollColor={'#ffffff'}
        className={'mx-5 py-4'}
        name={'PAIF'}
        left={
          <ul className="navbar-nav mr-auto ml-5">
            <li className="nav-item active">
              <a className="nav-link" href="/about-us"> About Us </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/discover"> Discover </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/projects"> Projects </a>
            </li>
          </ul>
        }
        right={
          <a href="/donate" className="btn btn-dark btn-block"> Donate </a>
        }
        mobileNav={
          <ul className="navbar-nav text-center">

            <li className="nav-item my-3 active">
              <a className="nav-link" href="/about-us"> About Us</a>
            </li>
            <li className="nav-item my-3">
              <a className="nav-link" href="/discover"> Discover </a>
            </li>
            <li className="nav-item my-3">
              <a className="nav-link" href="/projects">Gallery</a>
            </li> Projects
            <li className="nav-item my-3">
              <a href="/donate" className="btn btn-dark col-6"> Donate </a>
            </li>
          </ul>
        }
      ></TopNav >

      <div className="container">
        {isDesktopOrLaptop &&
          <div className='row mx-0 bg-white' style={{ height: '100vh' }}>
            <Image
              layout="fill"
              objectFit="contain"
              quality={100}
              src={'/img/bg/hero-bg.png'}
              alt={'Hero Background'}
            />
            <div className="col-lg-5 col-12 my-auto">
              <h1 className="display-4 font-weight-bold"> A Passion for Better Medicine. </h1>
            </div>
            {/* <div className="col-lg-7 col-12">
          </div> */}
          </div>
        }
        {isTabletOrMobile &&
          <div className='row mx-0 bg-white' style={{ height: '100vh' }}>
            <Image
              layout="fill"
              objectFit="cover"
              quality={100}
              src={'/img/bg/hero-mobile.jpg'}
              alt={'Hero Background Mobile'}
            />
            <div className="col-lg-5 col-12 m-auto">
              <h1 className="display-4 text-center text-white font-weight-bold"> A Passion for Better Medicine. </h1>
            </div>
          </div>
        }
      </div>

      {/* <div className="row mx-0 mt-5">
        <div className="my-auto">
          <img className="w-50" src="/img/left-icon.svg" />
        </div>

        <div className="col-10 px-0 mx-auto">
          <style global jsx>
            {`
              /* Styling Pagination*
                .owl-theme .owl-dots .owl-dot span {
                  -webkit-border-radius: 0;
                  -moz-border-radius: 0;
                  border-radius: 0;
                  width: 100px;
                  height: 5px;
                  margin-left: 2px;
                  margin-right: 2px;
                  background: #ccc;
                  border: none;
              }

              .owl-theme .owl-dots .owl-dot.active span,
              .owl-theme .owl-dots.clickable .owl-dot:hover span {
                  background: #3F51B5;
              }
            `}
          </style>
          <OwlCarousel
            showDots={true}
            showNavs={true}
            customDots={'custom-dots'}
            customNavs={{
              previous: 'custom-previous',
              next: 'custom-next'
            }}
          >
            <Card className="mb-4 agentCard" >
              <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
              <Card.Body>
                <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                  <h3 className="text-dark">{'agt.agent_name'}</h3>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_mobile'}</small>
                  </p>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_email'}</small>
                  </p>
                </div>
              </Card.Body>
            </Card>

            <Card className="mb-4 agentCard" >
              <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
              <Card.Body>
                <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                  <h3 className="text-dark">{'agt.agent_name'}</h3>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_mobile'}</small>
                  </p>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_email'}</small>
                  </p>
                </div>
              </Card.Body>
            </Card>

            <Card className="mb-4 agentCard" >
              <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
              <Card.Body>
                <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                  <h3 className="text-dark">{'agt.agent_name'}</h3>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_mobile'}</small>
                  </p>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_email'}</small>
                  </p>
                </div>
              </Card.Body>
            </Card>

            <Card className="mb-4 agentCard" >
              <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
              <Card.Body>
                <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                  <h3 className="text-dark">{'agt.agent_name'}</h3>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_mobile'}</small>
                  </p>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_email'}</small>
                  </p>
                </div>
              </Card.Body>
            </Card>

            <Card className="mb-4 agentCard" >
              <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
              <Card.Body>
                <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                  <h3 className="text-dark">{'agt.agent_name'}</h3>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_mobile'}</small>
                  </p>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_email'}</small>
                  </p>
                </div>
              </Card.Body>
            </Card>

            <Card className="mb-4 agentCard" >
              <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
              <Card.Body>
                <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                  <h3 className="text-dark">{'agt.agent_name'}</h3>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_mobile'}</small>
                  </p>
                  <p className="text-muted mb-0">
                    <small>{'agt.agent_email'}</small>
                  </p>
                </div>
              </Card.Body>
            </Card>
          </OwlCarousel>
        </div>
        <div className="my-auto">
          <a href="#" className="text-decoration-none" id="custom-next">
            <img className="w-50" src="/img/right-icon.svg" />
          </a>
        </div>
      </div>
     */}
    </Loader>

  )
}
